function build_and_run_test
    set -x SPECTRE_INEXT_PATH /space2/users/$USER/Projects/inext_hw_fe
    set -x SPECTRE_TEST_PATH $SPECTRE_INEXT_PATH/verif/scu/tests/sw/tests/scu_bootrom_test/score/cpu0
    set -x SPECTRE_RUN_PATH $SPECTRE_INEXT_PATH/target/scu_BOOTROM/runs

    set -x case_name $argv[1]
    set -x case_json (jq ".[] | select(.name == \"$case_name\")" release/bootrom_validation.json)
    echo "Test case:"
    echo $case_json | jq
    set -x vcs_test_args (echo $case_json | jq -r '.vcs_test_args')

    if not which deactivate >/dev/null 2>/dev/null
        source venv/bin/activate.fish
    end
    set -x release_cli "python release/generate_bootrom_validation.py --toolchain-path /opt/syntacore-gcc/bin/ --case $case_name release/bootrom_validation.json"
    eval $release_cli
    if test -e bootrom_validation/$case_name/bootrom.bits
        ln -fs ../bootrom.elf bootrom_validation/$case_name/bootrom.original.elf
    else
        for f in bootrom.{bits,elf,bin}
            ln -fs ../$f bootrom_validation/$case_name/$f
        end
    end

    gum spin --show-output --title "Copying test files..." -- scp -r bootrom_validation/$case_name/ spectre.il.nextsilicon.com:$SPECTRE_TEST_PATH/
    set -x next_run_line "nextRun.py -cte scu -cte_mode BOOTROM -t scu_bootrom_test -test_args \"+test=$case_name +tracelog=1 +tracelog_time=1 +tracelog_file=./trace +flash_name=flash_bootrom +test_to=45000 +dump_vcd +UVM_VERBOSITY=UVM_NONE $vcs_test_args\" -pre_simv_cmd \"\$REPO_ROOT/verif/scu/tests/sw/tests/scu_bootrom_test/score/cpu0/prerun.csh $case_name\" -dump"
    echo -e "Final line:\n$next_run_line\n"
    gum confirm "Run this command on spectre?"
    if test 0 -ne "$status"
        echo "Aborting!"
        return
    end
    set -x run_dir (gum spin --show-output --title "Running test on spectre..." -- ssh spectre.il.nextsilicon.com "\
      touch /tmp/\$USER\_stamp
      zellij action new-tab -n $case_name
      zellij ac write-chars 'cd $SPECTRE_INEXT_PATH'
      zellij ac write 13  # Newline
      zellij ac write-chars 'tcsh'
      zellij ac write 13  # Newline
      zellij ac write-chars 'source scripts/next_setup.csh'
      zellij ac write 13  # Newline
      zellij ac write-chars '$next_run_line'
      zellij ac write 13
      while export new_dir=\$(find $SPECTRE_RUN_PATH -maxdepth 1 -type d -newer /tmp/\$USER\_stamp -and -not -samefile $SPECTRE_RUN_PATH);
        test -z \"\$new_dir\"
        do sleep 0.5
      done
      zellij run --cwd \$new_dir -- bash -c 'while test ! -e score_trace_core0.log; do sleep 1; done; tail -f score_trace_core0.log'
      echo \$new_dir")
    echo "Run dir: $SPECTRE_RUN_PATH/$run_dir"

    gum confirm "Create JIRA ticket?"
    if test 0 -eq "$status"
        create_jira_ticket --nxt-dir=(pwd) \
            --case-dir=(realpath bootrom_validation/$case_name) \
            --case-name=$case_name \
            --run-dir=$SPECTRE_RUN_PATH/$run_dir \
            --release-cli=$release_cli
    end
end
