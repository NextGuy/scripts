# `fish` functions

| Function | Purpose | Dependencies |
|----------|---------|--------------|
|[create_jira_ticket](create_jira_ticket.fish)|After building *and running* a bootrom validation test, this automatically creates a JIRA ticket describing the build properties, the run directory and uploads all the right artifacts | [jira-cli](https://github.com/ankitpokhrel/jira-cli), [gum](https://github.com/charmbracelet/gum)|
|[kitty_ssh](kitty_ssh.fish)|`ssh` into a server and have its name written on-screen so you remember where you are :)|[kitty](https://github.com/kovidgoyal/kitty), [imagemagick](https://www.imagemagick.org/), a file in ~/Pictures/TerminalEmpty.png with an empty background :(|
|[build_and_run_test](build_and_run_test.fish|In the nxt009, build the test case named by the argument, upload its artifacts to `spectre`, open a new `zellij` tab there, use it to run the test, track the logs in a new pane and optionally create a JIRA ticket|[gum](https://github.com/charmbracelet/gum), [zellij](https://github.com/zellij-org/zellij) on `spectre`|
