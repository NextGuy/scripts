function create_jira_ticket
    argparse h/help 'nxt-dir=' 'case-dir=' 'case-name=' 'run-dir=' 'release-cli=' -- $argv

    if set -ql _flag_help
        echo "Usage: create_jira_ticket [-h/--help] [--nxt-dir=DIR --case-dir=DIR] [--case-name=NAME] [--run-dir=REMOTE_DIR] [--release-cli=STR]"
        return
    end

    if set -ql _flag_nxt_dir; and not set -ql _flag_case_dir;
        or not set -ql _flag_nxt_dir; and set -ql _flag_case_dir
        echo "Either specify both or none of --nxt-dir and --case-dir" >2
        return
    end

    if set -ql _flag_nxt_dir
        set -x NXT009_DIR $_flag_nxt_dir
        set -x CASE_DIR $_flag_case_dir
    else
        if test ! -d ../../bootrom
            echo "Run from validation case dir or pass --nxt-dir and --case-dir" >2
            return
        end
        set -x NXT009_DIR ../../
        set -x CASE_DIR (pwd)
    end

    if set -ql _flag_case_name
        set -x test_name $_flag_case_name
    else
        set -x test_name (basename $CASE_DIR)
    end

    if not set -q SPECTRE_RUN_PATH
        set -x SPECTRE_RUN_PATH /space2/users/guyp/Projects/inext_hw_fe/target/scu_BOOTROM/runs
    end

    if not set -ql _flag_release_cli
        set -x release_cli (history search --max 1 generate_bootrom_validation.py)
    else
        set -x release_cli $_flag_release_cli
    end

    if not set -ql _flag_run_dir
        set -x run_dir $SPECTRE_RUN_PATH/(gum spin --show-output --title "Fetching run dir..." -- ssh spectre.il.nextsilicon.com "ls -t $SPECTRE_RUN_PATH | head -1")
    else
        set -x run_dir $_flag_run_dir
    end

    set -x nxt_hash (git -C $NXT009_DIR/ show-ref -s HEAD)
    while not git -C $NXT009_DIR/ diff-index --quiet --ignore-submodules HEAD
        gum confirm "There are uncommitted changes in $NXT009_DIR" --affirmative="Continue anyway" --negative="Try again"
        and break
    end

    if not git -C $NXT009_DIR/ diff-index --quiet --ignore-submodules HEAD
        set -x nxt_hash "$nxt_hash (modified)"
    end

    set -x bootrom_hash (git -C $NXT009_DIR/bootrom show-ref -s HEAD)
    while not git -C $NXT009_DIR/bootrom diff-index --quiet --ignore-submodules HEAD
        gum confirm "There are uncommitted changes in $NXT009_DIR/bootrom" --affirmative="Continue anyway" --negative="Try again"
        and break
    end

    if not git -C $NXT009_DIR/bootrom diff-index --quiet --ignore-submodules HEAD
        set -x bootrom_hash "$bootrom_hash (modified)"
    end
    set -x body "Test $test_name in test plan - https://docs.google.com/spreadsheets/d/1wT442lbDK-HeuB1aIqq0NiRLk2baDS-7l2nzosQ2yqk/edit#gid=0
Release CLI used: `$release_cli`  
nxt009 commit: https://github.com/nextsilicon/nxt009-soc-sw/commit/$nxt_hash  
bootrom commit: https://github.com/nextsilicon/bootrom/commit/$bootrom_hash  
Running in: $run_dir  
(This issue was generated automatically)"

    set -x issue_title "Bootrom validation - $test_name"
    gum confirm "$body

Would you like to edit the body?"
    if test "$status" -eq 0
        set -x body (echo $body | vipe)
    end
    echo -e "$issue_title\n\n$body\n"
    gum confirm "Create this issue?"
    if test "$status" -ne 0
        echo Aborting
        return
    end

    set -x jira_user (jira me)
    set -x issue_labels Bootchain,Bootrom,gen2
    set -x issue_url (echo $body | jira issue create -tTask --template - -s"Bootrom validation - $test_name" -a $jira_user -l $issue_labels)
    echo $issue_url
    set -x issue_code (echo $issue_url | sed 's|.*browse/\(.*\)|\1|')
    set -x attachment_files $CASE_DIR/*
    if not test -e $CASE_DIR/bootrom.bits
        set -x attachment_files $attachment_files $CASE_DIR/../bootrom.*
    end
    set -x attachment_flags '-F file=@'(string join -- ' -F file=@' $attachment_files)
    gum spin --title "Uploading attachments..." -- curl -u "$jira_user:$JIRA_API_TOKEN" -X POST -H "X-Atlassian-Token: no-check" (string split " " -- $attachment_flags) https://nextsilicon.atlassian.net/rest/api/2/issue/$issue_code/attachments
end
