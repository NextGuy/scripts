
function kitty_ssh
    set target_hostname $argv[1]
    ssh $target_hostname &
    set name (echo $target_hostname | cut -d. -f1)
    set resolution (kitty +kitten icat --print-window-size)
    set width (echo $resolution | cut -dx -f1)
    set height (echo $resolution | cut -dx -f2)
    set name_width (math (echo $name | wc -c) "*  30")
    set name_start (math -s 0 "$width * 0.92 - $name_width")
    convert -pointsize 60 -fill grey -draw "text $name_start,110 \"$name\"" ~/Pictures/TerminalEmpty.png /tmp/term.png
    kitty @ set-background-image --layout=scaled /tmp/term.png
    fg
    kitty @ set-background-image --layout=scaled ~/Pictures/TerminalEmpty.png
end

