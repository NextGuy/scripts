
function checkout_main_branch
    set -x options main master
    for option in $options
        if git rev-parse --verify $option &>/dev/null
            echo "git checkout $option"
            return 0
        end
    end
    return 1
end
