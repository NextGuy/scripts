function rtl_debug
    pushd ~/Projects/bootrom/rtl_debugging/
    set -x HWSIM_RUN_DIR $argv[1]
    if test -n "$argv[2]"
        set dirbase "$argv[2]"
    else
        set dirbase (date +%d_%m_%y)
    end
    if test -d $dirbase
        set dir_name "$dirbase"_(echo "$dirbase"* | wc -w)
    else
        set dir_name $dirbase
    end
    mkdir $dir_name
    cd $dir_name
    scp spectre.il.nextsilicon.com:$HWSIM_RUN_DIR/{bootrom\*.elf,score_trace_core0.log,dump.vcd,test.log} ./
    set -x test_name (head -1 test.log | sed 's/.*+test=\([^ ]\+\).*/\1/')
    zellij action rename-tab "$test_name"
    if not which deactivate >/dev/null 2>/dev/null
        source ../../venv/bin/activate.fish
    end
    python ~/Projects/nxt009-soc-sw/scripts/score_log.py --elf bootrom*.elf --input-log score_trace_core0.log log_enrich
end
