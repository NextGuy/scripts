#!/bin/bash

# For every display, find the aspect ratio whose quotient is closest to the resolution quotient

ratio_differences() {
	resolutionX=$1
	resolutionY=$2
	aspect_ratios=$3

	for ratio in $aspect_ratios; do
		export aspect_ratioX=$(echo $ratio | cut -d: -f1)
		export aspect_ratioY=$(echo $ratio | cut -d: -f2)
		echo "$aspect_ratioX $aspect_ratioY $resolutionX $resolutionY $ratio" | awk '{printf "%f " $5 "\n", sqrt(($1 / $2 - $3 / $4)^2)}'
	done
}

match_ratio() {
	ratio_differences $1 $2 "$3" | sort -n | head -1 | cut -f2 -d' '
}

main() {
	i=0
	OLDIFS=$IFS
	IFS=$'\n'
	for display_line in $(xrandr --listactivemonitors | sed 1d); do
		IFS=$OLDIFS
		echo "display_line: $display_line"
		resolution=$(echo $display_line | awk '{print $3}')
		resX=$(echo $resolution | cut -dx -f1 | cut -d/ -f1)
		resY=$(echo $resolution | cut -dx -f2 | cut -d/ -f1)
		display_name=$(echo $display_line | awk '{print $2}' | grep -o "[^\*\+]*")
		head=$(echo $display_line | cut -d: -f1)
		aspect_ratios=$(ls ~/Pictures/Wallpapers)
		best_ratio=$(match_ratio $resX $resY "$aspect_ratios")
		echo "Setting $display_name (index $i, resolution $resolution, ratio $best_ratio)"
		nitrogen --head=$head --set-scaled --random ~/Pictures/Wallpapers/$best_ratio
		i=$(($i+1))
	done
}

main
